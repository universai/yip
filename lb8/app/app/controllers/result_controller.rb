# frozen_string_literal: true

# Controller for Result
class ResultController < ApplicationController
  def index
    render(file: "#{Rails.root}/public/400.html", status: 400) unless params.key? :t_number
    @t_n = params[:t_number].to_i
    @num_list = []
    (2..@t_n).each do |n|
      r = ResultHelper.friend?(n)
      @num_list << r if r
    end
  end
end
