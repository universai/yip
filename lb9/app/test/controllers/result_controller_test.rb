# frozen_string_literal: true

require 'test_helper'

class ResultControllerTest < ActionDispatch::IntegrationTest
  test 'should not get empty index' do
    get result_index_url
    assert_response 400
  end

  test 'should response to index' do
    get '/result?t_number=284'
    assert_response :success
  end
end
