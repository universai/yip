# frozen_string_literal: true

# Record for Application
class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
end
