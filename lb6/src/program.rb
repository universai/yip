# frozen_string_literal: true

# PartOne is for part one
module PartOne
  # @param eps [Float]
  # @return [Array(Float, Integer)]
  def self.calculate_curve_length(eps)
    num_del = 1
    loop do
      case try_iterate_with_step((2 - 1) / num_del.to_f, eps)
      in :fail
        num_del += 1
        redo
      in result
        return [result, num_del]
      end
    end
  end

  # @param xss [Array(Integer, Integer)]
  # @return [Float]
  def self.calculate_distance(*xss)
    coor_a, coor_b = xss.map { |o_x| [o_x, Math.log(o_x)] }
    Math.sqrt((coor_b.first - coor_a.first)**2 + (coor_b.last - coor_a.last)**2)
  end

  # @param num_del [Integer]
  # @param eps [Float]
  # @return [Symbol | Float]
  def self.try_iterate_with_step(step, eps, leng = 0)
    # @param a_x [Float]
    (1.0...2.0).step(step).map do |a_x|
      dist = calculate_distance(a_x, a_x + step)
      return :fail if dist > eps

      leng += dist
    end
    leng
  end
end

# PartTwo is for part two
module PartTwo
  # @param eps [Float]
  # @return [Array(Float, Integer)]
  def self.calculate_curve_length(eps, num_del = 1)
    (Enumerator.new do |elem|
       loop do
         elem << [PartOne.try_iterate_with_step((2 - 1) / num_del.to_f, eps), num_del]
         num_del += 1
       end
     end).find { |elem| elem.first != :fail }
  end
end

# PartThree is for part three
module PartThree
  # @param opts [Hash<Symbol, Float>]
  # @param func [Proc]
  # @return [Float]
  def self.intprg(opts, func = nil, &block)
    func ||= block
    # @type [Float]
    a_o, b_o, n_o = [opts[:a], opts[:b], opts[:n], opts]
    backstep = (a_o - b_o) / n_o.to_f
    ((b_o...a_o).step(backstep).map { |c_x| func.call(c_x) }).sum * backstep.abs
  end
end
