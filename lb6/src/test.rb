# frozen_string_literal: true

require 'minitest/autorun'

require_relative './program'

# Unit testing
class ProgramTest < Minitest::Test
  def test_part_one
    etal = 1.222016177086634
    (1..5).each do |delt|
      delt = 10**-delt
      assert_in_delta(PartOne.calculate_curve_length(delt).first, etal, delt)
    end
  end

  def test_part_two
    etal = 1.222016177086634
    (1..5).each do |delt|
      delt = 10**-delt
      assert_in_delta(PartTwo.calculate_curve_length(delt).first, etal, delt)
    end
  end

  # This code smell of :reek:NestedIterators
  # This code smell of :reek:TooManyStatements
  def test_part_three
    etal_one = 1.125386083083269
    etal_two = 0.666666666666666
    (1...5).each do |delt|
      delt_dn = 10**-delt
      delt_up = 30**delt
      assert_in_delta(PartThree.intprg({ a: 0, b: 1, n: delt_up }, ->(x_d) { (Math::E**x_d) / (x_d + 1) }),
                      etal_one, delt_dn)
      assert_in_delta(PartThree.intprg({ a: 0, b: 2, n: delt_up }) { |x_v| x_v * (x_v - 1) }, etal_two, delt_dn)
    end
  end
end
