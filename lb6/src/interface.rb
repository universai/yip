# frozen_string_literal: true

require_relative './program'
require 'English'

loop do
  puts "Выберите номер задания: [1], [2], [3]\nИли напишите [выход], чтобы выйти"
  case gets.chomp
  when '1'
    res = [PartOne.calculate_curve_length(10**-3), PartOne.calculate_curve_length(10**-4)]
    puts "Длина прямой ln(x)[1, 2] есть: \n" \
    "#{res.first[0]} посчитанная за #{res.first[1]} итераций при точности #{10**-3}\n" \
    "#{res.last[0]} посчитанная за #{res.last[1]} итераций при точности #{10**-4}"
  when '2'
    res = [PartTwo.calculate_curve_length(10**-3), PartTwo.calculate_curve_length(10**-4)]
    puts "Длина прямой ln(x)[1, 2] есть: \n" \
    "#{res.first[0]} посчитанная за #{res.first[1]} итераций при точности #{10**-3}\n" \
    "#{res.last[0]} посчитанная за #{res.last[1]} итераций при точности #{10**-4}"
  when '3'
    puts 'Введите число разбиений: '
    puts 'Значение первого интеграла: '\
    "#{PartThree.intprg({ a: 0, b: 1, n: gets.to_i }, ->(x_v) { (Math::E**x_v) / (x_v + 1) })}\n" \
    'Значение второго интеграла: ' \
    "#{PartThree.intprg({ a: 0, b: 2, n: $LAST_READ_LINE.to_i }) { |x_v| x_v * (x_v - 1) }}"
  when 'выход'
    puts 'Завершаю работу'
    break
  else
    puts 'Некорректный выбор'
  end
  puts('=' * 15)
end
