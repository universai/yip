# frozen_string_literal: true

Rails.application.routes.draw do
  root 'mainpage#index'

  get 'result', to: 'result#index'
end
