import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  connect() {
    this.element.textContent = "Загрузка...";
    let number = this.element.dataset.number

    let xhttp = new XMLHttpRequest();

    xhttp.open('GET', `http://127.0.0.1:3000/result.xml?t_number=${number}&xslt=true`, false);
    xhttp.send('');
    let xml = xhttp.responseXML

    xhttp.open('GET', `http://127.0.0.1:3000/result_transformer.xslt`, false);
    xhttp.send('')
    let xslt = new XSLTProcessor;
    xslt.importStylesheet(xhttp.responseXML)

    this.element.innerHTML = ""
    this.element.append(xslt.transformToFragment(xml, document))
  }
}
