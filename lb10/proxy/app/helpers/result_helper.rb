# frozen_string_literal: true

# R
module ResultHelper
  def self.get_xml_to_html(t_number)
    xml = Nokogiri::XML(Net::HTTP
    .get_response(URI("http://127.0.0.1:3000/result.xml?t_number=#{t_number}&xslt=true")).body)
    Nokogiri::XSLT(Net::HTTP
    .get_response(URI('http://127.0.0.1:3000/result_transformer.xslt')).body).transform(xml)
  end
end
