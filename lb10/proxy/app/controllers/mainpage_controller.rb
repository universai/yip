# frozen_string_literal: true

# Mainpage contorller
class MainpageController < ApplicationController
  def index; end
end
