# frozen_string_literal: true

require 'net/http'
require 'uri'

# Result Controller
class ResultController < ApplicationController
  def index
    render(file: "#{Rails.root}/public/400.html", status: 400) unless params.key? :t_number
    @type = params[:type]&.to_sym || :ssr
    @t_number = params[:t_number]
    return unless @type == :ssr

    @resp = ResultHelper.get_xml_to_html(params[:t_number]).to_html.html_safe
  end
end
