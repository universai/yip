# frozen_string_literal: true

require 'test_helper'

class ResultControllerTest < ActionDispatch::IntegrationTest
  test 'should not get empty index' do
    get '/result'
    assert_response 400
  end

  test 'should response to index' do
    get '/result?t_number=284'
    assert_response :success
  end

  test 'ssr must be loaded' do
    get '/result?t_number=2000'
    assert_response :success
    first_len = @response.body.size

    get '/result?t_number=2000&type=csr'
    assert first_len > @response.body.size
  end
end
