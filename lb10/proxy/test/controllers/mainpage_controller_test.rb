# frozen_string_literal: true

require 'test_helper'

class MainpageControllerTest < ActionDispatch::IntegrationTest
  test 'should load' do
    get '/'
    assert_response :success
  end
end
