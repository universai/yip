# frozen_string_literal: true

# Controller for Result
class ResultController < ApplicationController
  def index
    render(file: "#{Rails.root}/public/400.html", status: 400) unless params.key? :t_number
    @t_n = params[:t_number].to_i
    @num_list = (2..@t_n).map { |n| ResultHelper.friend?(n) }.select { |x| x }
    @xlst = (params.key? :xslt) && (params[:xslt] == 'true')
  end
end
