# frozen_string_literal: true

# Helper for Result
module ResultHelper
  # @param num [Integer]
  # @return [Boolean]
  def self.friend?(num)
    friend_num = friend_parse(num)
    friend_fr = friend_parse(friend_num) unless friend_num.zero?
    if (defined? friend_fr) && (num == friend_fr) && (friend_num < num)
      [friend_num, num]
    else
      false
    end
  end

  # @param num [Integer]
  # @return [Integer]
  def self.friend_parse(num)
    ((1...num).select { |cndt| (num % cndt).zero? }).sum
  end
end
