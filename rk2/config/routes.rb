# frozen_string_literal: true

Rails.application.routes.draw do
  get 'input', to: 'head#input', as: 'input'
  get 'result', to: 'head#result', as: 'result'

  root 'head#input'
end
