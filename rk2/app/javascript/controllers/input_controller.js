import { Controller } from "@hotwired/stimulus"
import 'jquery'

export default class extends Controller {
  connect() {
    let spattr = "^(-?\\d+(\\.\\d+)?,\\s)*(-?\\d+(\\.\\d+)?)$"
    let pattr = RegExp(spattr)
    let formt = "[число], [целая.дробная], [-число]..."
    $("#arr_z")
      .attr('placeholder', `Формат ввода: ${formt}`)
      .attr('pattern', spattr)
      .on('invalid', function (event) {
        event.target.setCustomValidity('');
        if (event.target.validity.patternMismatch) {
          event.target.setCustomValidity(`Пожалуйтса, соблюдайте формат ввода данных: ${formt}`);
        }
      }).val('43, 342.43, -13, -432.32, 12')
  }
}
