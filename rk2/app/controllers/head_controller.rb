# frozen_string_literal: true

# Head contrl
class HeadController < ApplicationController
  def input; end

  def result
    @err = nil
    @list = nil
    if params.key? :arr_z
      res = HeadHelper.make params[:arr_z]
      @list = params[:arr_z].split(', ')
      @err = res.last
      @list[res.first] = res[1] unless @err
    else
      @err = 'Вы не передали список'
    end
  end
end
