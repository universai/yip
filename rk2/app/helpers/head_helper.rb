# frozen_string_literal: true

# head helper
module HeadHelper
  def self.make(s_list)
    unless Regexp.new('^(-?\\d+(\\.\\d+)?,\\s)*(-?\\d+(\\.\\d+)?)$').match? s_list
      return [nil,
              'Неверный формат массива']
    end

    # @type [Array<Float>]
    list = s_list.split(', ').map(&:to_f)
    indk3 = list.find_index { |x| (x % 3).zero? }
    return [nil, nil, 'Нет элементов кратных 3'] unless indk3

    min = list.min
    indmin = list.find_index { |x| x == min }
    [indmin, indk3, nil]
  end
end
