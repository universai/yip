# frozen_string_literal: true

# kfsdojfi
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
