# frozen_string_literal: true

require 'test_helper'

class HeadControllerTest < ActionDispatch::IntegrationTest
  test 'should get input' do
    get head_input_url
    assert_response :success
  end

  test 'should get result' do
    get head_result_url
    assert_response :success
  end
end
