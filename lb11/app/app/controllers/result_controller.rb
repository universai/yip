# frozen_string_literal: true

# Controller for Result
class ResultController < ApplicationController
  def index
    render(file: "#{Rails.root}/public/400.html", status: 400) unless params.key? :t_number
    @t_n = params[:t_number].to_i
    @num_list = ResultHelper.get_friends(@t_n)
    @xlst = (params.key? :xslt) && (params[:xslt] == 'true')
  end

  def ultimate
    @pairs = Pair.all.order(one: :asc).to_a
  end
end
