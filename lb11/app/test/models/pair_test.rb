# frozen_string_literal: true

require 'test_helper'

# Test Pair
class PairTest < ActiveSupport::TestCase
  test 'miracle of creation' do
    Pair.create(one: 220, two: 284, max_valid_rn: 285)
    assert_equal 1, Pair.count
  end

  test 'miracle of existance' do
    Pair.create(one: 220, two: 284, max_valid_rn: 285)
    assert_equal 1, Pair.where(one: 220).count
    assert_equal 0, Pair.where(two: 283).count
    assert_equal 1, Pair.where(max_valid_rn: 285).count
  end

  test 'only unique' do
    Pair.create(one: 220, two: 284, max_valid_rn: 285)
    assert_equal 1, Pair.count
    Pair.create(one: 220, two: 284, max_valid_rn: 285)
    assert_equal 1, Pair.count
    Pair.create(one: 223, two: 284, max_valid_rn: 285)
    assert_equal 1, Pair.count
    Pair.create(one: 220, two: 286, max_valid_rn: 285)
    assert_equal 1, Pair.count
    Pair.create(one: 223, two: 286, max_valid_rn: 222)
    assert_equal 1, Pair.count
  end
end
