# frozen_string_literal: true

Rails.application.routes.draw do
  root 'input#index', as: 'input_index'

  get 'result', to: 'result#index', as: 'result_index'
  get 'result/ultimate', to: 'result#ultimate', defaults: { format: :xml }
end
