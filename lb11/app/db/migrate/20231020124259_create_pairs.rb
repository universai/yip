# frozen_string_literal: true

# Create Pairs
class CreatePairs < ActiveRecord::Migration[7.0]
  def change
    create_table :pairs do |t|
      t.integer :one
      t.integer :two
      t.boolean :finalized
      t.integer :max_valid_rn

      t.timestamps
    end
  end
end
