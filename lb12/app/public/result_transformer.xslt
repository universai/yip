<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/pairs">
        <html>
            <table class="result">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Число 1</th>
                        <th>Число 2</th>
                    </tr>
                </thead>
                <tbody>
                    <xsl:for-each select="pair">
                        <tr>
                            <td>
                                <xsl:value-of select="@index" />
                            </td>
                            <td>
                                <xsl:value-of select="el[1]" />
                            </td>
                            <td>
                                <xsl:value-of select="el[2]" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </html>
    </xsl:template>
    <xsl:template match="/empty">
        <p class="result">К сожалению, на данном промежутке нет ни одной пары дружественных чисел</p>
    </xsl:template>
</xsl:stylesheet>
