# frozen_string_literal: true

# User model
class User < ApplicationRecord
  validates :login, presence: true, uniqueness: true
  validates :password, presence: true
  validate :requirements

  def requirements
    a = [
      /^[\w@\d]+$/.match?(login),
      /^[\w@!\d]+$/.match?(password),
      /[A-Z]/ =~ password,
      /\d/ =~ password,
      /[!_@]/ =~ password,
      /\w/ =~ password
    ]
    errors.add(:id, 'HTML page mismatch') unless a.all?
  end
end
