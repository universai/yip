# frozen_string_literal: true

# Result pair record
class Pair < ApplicationRecord
  validates :one, uniqueness: true, presence: true, \
                  comparison: { greater_than_or_equal_to: 2, less_than: :two }
  validates :two, uniqueness: true, presence: true, \
                  comparison: { greater_than_or_equal_to: 2 }
  validates :max_valid_rn, presence: true, comparison: { greater_than_or_equal_to: :one }

  def finalized?
    finalized
  end

  before_create do
    self.finalized ||= false
  end

  after_create do
    Pair.where(one: ...one, finalized: false).update(finalized: true)
  end

  def to_a
    [one, two]
  end
end
