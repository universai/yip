# frozen_string_literal: true

# User controller
class UsersController < ApplicationController
  before_action do
    reset_session if session.key?(:uid) && User.where(id: session[:uid]).first.nil?
    if %i[create auth].include?(params[:action].to_sym) && !%i[login password].all? { |k| params.key? k }
      render(file: "#{Rails.root}/public/400.html", status: 400)
    elsif !session.key?(:uid) && %i[list lk].include?(params[:action].to_sym)
      redirect_to newbie_path
    elsif session.key?(:uid) && !%i[logout lk update available list].include?(params[:action].to_sym)
      redirect_to '/'
    end
  end

  def update
    User.where(id: session[:uid]).update(login: params[:login], password: params[:password])
    redirect_to lk_path
  end

  def lk
    @u = User.where(id: session[:uid]).first
  end

  def logout
    reset_session
    redirect_to '/'
  end

  def login; end

  def auth
    u = User.where(login: params[:login]).first
    return render 'auth', status: 401 unless u&.password == params[:password]

    session[:uid] = u.id
    redirect_to input_index_path
  end

  def regf; end

  def create
    pr = User.create(password: params[:password], login: params[:login])
    if pr.valid?
      session[:uid] = pr.id
      redirect_to input_index_path
    else
      head 500
    end
  end

  def available
    if !params.key?(:login)
      render(file: "#{Rails.root}/public/400.html", status: 400)
    elsif !User.where(login: params[:login]).first.nil?
      head 409
    else
      params[:login] == 'admin' ? head(409) : head(200)
    end
  end

  def list
    @u = User.where(id: session[:uid]).first
    @us = User.all
  end
end
