# frozen_string_literal: true

# Controller for Result
class ResultController < ApplicationController
  before_action do
    reset_session if User.where(id: session[:uid]).first.nil?
    redirect_to newbie_path unless session.key?(:uid)
  end

  def index
    render(file: "#{Rails.root}/public/400.html", status: 400) unless params.key? :t_number
    @t_n = params[:t_number].to_i
    @num_list = ResultHelper.get_friends(@t_n)
    @xlst = (params.key? :xslt) && (params[:xslt] == 'true')
    @u = User.where(id: session[:uid]).first
  end

  def ultimate
    @pairs = Pair.all.order(one: :asc).to_a
    @u = User.where(id: session[:uid]).first
  end
end
