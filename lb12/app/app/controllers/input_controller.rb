# frozen_string_literal: true

# Controller for Input
class InputController < ApplicationController
  before_action do
    reset_session if User.where(id: session[:uid]).first.nil?
    redirect_to newbie_path unless session.key?(:uid)
  end

  def index
    @u = User.where(id: session[:uid]).first
  end
end
