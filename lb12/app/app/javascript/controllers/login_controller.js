import { Controller } from "@hotwired/stimulus"
import 'jquery'

export default class extends Controller {
  connect() {

    $('form').submit(function () {
      let errors = false;

      $('form div.error').hide()
      $('form input').removeClass('error')

      let login = $('form #login')

      let xhttp = new XMLHttpRequest();
      xhttp.open('GET', `/users/available?login=${login.val()}`, false);
      xhttp.send('');
      let login_exist = xhttp.status == 409;

      if (!login_exist) {
        errors = true;
        login.addClass("error")
        $("#login_error").show().html('Такой логин не существует')
      }

      return !errors;
    })

    $('.pwdasset').height($('form input').height())
    let main = $('.pwdasset').attr('src')
    let subst = $('.pwdasset').data('subst')
    let pwd = $('input[type="password"]')
    $('.pwdasset').hover(function () {
      $('.pwdasset').attr('src', subst)
      pwd.attr('type', 'text')
    }, function () {
      $('.pwdasset').attr('src', main)
      pwd.attr('type', 'password')
    })
  }
}
