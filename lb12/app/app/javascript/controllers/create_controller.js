import { Controller } from "@hotwired/stimulus"
import 'jquery'

export default class extends Controller {
  connect() {

    $('form').submit(function () {
      let errors = false;

      $('form div.error').hide()
      $('form input').removeClass('error')

      let login = $('form #login')
      let password = $('form #password')
      let passwordc = $('form #passwordc')

      let xhttp = new XMLHttpRequest();
      xhttp.open('GET', `/users/available?login=${login.val()}`, false);
      xhttp.send('');
      let login_av = xhttp.status == 200;

      if (!login_av) {
        errors = true;
        login.addClass("error")
        $("#login_error").show().html('Такой логин занят')
      } else if (!login.val().match(/^[\w@\d]+$/)) {
        errors = true;
        login.addClass("error")
        $("#login_error").show().html('Логин может содержать только цифры, латинские буквы а так же символы \'_\' и \'@\'')
      } else if (!password.val().match(/^[\w@!\d]+$/)) {
        errors = true;
        password.addClass("error")
        $("#password_error").show().html('Пароль может содержать только цифры, латинские буквы а так же символы \'_\', \'@\' и \'!\'')
      } else if (!password.val().match(/[A-Z]/) || !password.val().match(/\d/) || !password.val().match(/[!_@]/) || !password.val().match(/\w/)) {
        errors = true;
        password.addClass("error")
        $("#password_error").show().html('В пароле хотя бы раз должны встречаться заглавная буква, строчная буква, цифра и спецсимвол')
      } else if (password.val() != passwordc.val()) {
        errors = true;
        password.addClass('error')
        passwordc.addClass('error')
        $("#passwordc_error").show().html('Пароли должны совпадать!')
      }

      return !errors;
    })

    $('.pwdasset').height($('form input').height())
    let main = $('.pwdasset').attr('src')
    let subst = $('.pwdasset').data('subst')
    let pwd = $('input[type="password"]')
    $('.pwdasset').hover(function () {
      $('.pwdasset').attr('src', subst)
      pwd.attr('type', 'text')
    }, function () {
      $('.pwdasset').attr('src', main)
      pwd.attr('type', 'password')
    })
  }
}
