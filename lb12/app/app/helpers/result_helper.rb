# frozen_string_literal: true

# Helper for Result
module ResultHelper
  # @param num [Integer]
  # @return [Boolean | Array(Integer, Integer)]
  def self.get_friends(num)
    edge = -> { Pair.where(finalized: false).first }
    (2..num).each { |x| friend?(x) } unless edge.call
    return [] unless edge.call

    edge = validate_edge(num, edge)
    res = Pair.where(finalized: true, two: ..num).order(one: :asc).to_a.map(&:to_a)
    edge.two <= num ? res.push(edge.to_a) : res
  end

  # @param num [Integer]
  # @param edge [Proc(Pair)]
  # @return [Pair]
  def self.validate_edge(num, edge)
    (edge.call.max_valid_rn..num).each do |itr|
      friend?(itr)
    end
    edge.call.update(max_valid_rn: num) if num > edge.call.max_valid_rn
    edge.call
  end

  # @param num [Integer]
  # @param m_valid [Integer]
  # @return [Boolean | Array(Integer, Integer)]
  def self.friend?(num)
    friend_num = friend_parse(num)
    friend_fr = friend_parse(friend_num) unless friend_num.zero?
    if (defined? friend_fr) && (num == friend_fr)
      friend_num, num = num, friend_num if friend_num > num
      Pair.create(one: friend_num, two: num, max_valid_rn: friend_num)
      true
    else
      false
    end
  end

  # @param num [Integer]
  # @return [Integer]
  def self.friend_parse(num)
    r = []
    (1..Math.sqrt(num).round).each do |n|
      next unless (num % n).zero?

      r.push(n)
      e = (num / n).round
      r.push(e) unless (e == num) || (e == n)
    end
    r.sum
  end
end
