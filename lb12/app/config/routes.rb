# frozen_string_literal: true

Rails.application.routes.draw do
  root 'input#index', as: 'input_index'

  get 'result', to: 'result#index', as: 'result_index'
  get 'result/ultimate', to: 'result#ultimate', defaults: { format: :xml }, as: 'ultimate'

  get 'users/create', to: 'users#regf', as: 'newbie'
  get 'users/login', to: 'users#login', as: 'login'
  get 'users/available', to: 'users#available'
  get 'users/logout', to: 'users#logout', as: 'logout'
  get 'users/lk', to: 'users#lk', as: 'lk'
  get 'users/list', to: 'users#list'
  post 'users/lk', to: 'users#update'
  post 'users/create', to: 'users#create', as: 'create'
  get 'users', to: 'users#auth', as: 'auth'
end
