# frozen_string_literal: true

require 'test_helper'

class InputControllerTest < ActionDispatch::IntegrationTest
  setup do
    get auth_path, params: { login: :test, password: 'Te5!' }
    assert_response :redirect
  end
  test 'should get index' do
    get input_index_url
    assert_response :success
  end
end
