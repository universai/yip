# frozen_string_literal: true

require 'test_helper'

class NewbieFlowsTest < ActionDispatch::IntegrationTest
  test 'circle of life' do
    get '/'
    assert_response :redirect
    follow_redirect!

    assert_equal newbie_path, path
    assert_select "form[action='#{newbie_path}']", 1

    post create_path, params: { login: 'mike', password: 'M!k3' }
    assert_response :redirect
    follow_redirect!

    assert_equal '/', path
    assert_select "a[href='#{lk_path}']", 'mike'

    get result_index_path, params: { t_number: 3000 }
    assert_select 'table.result', 1
  end

  test 'rogue' do
    get result_index_path, params: { t_number: 3000 }
    assert_response :redirect

    get ultimate_path
    assert_response :redirect
  end
end
