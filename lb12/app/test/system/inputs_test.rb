# frozen_string_literal: true

require 'application_system_test_case'

class InputsTest < ApplicationSystemTestCase
  test 'visiting the index' do
    visit login_path
    find('#login').fill_in with: 'test'
    find('#password').fill_in with: 'Te5!'
    find('input[type="submit"]').click

    sleep 10 until page.has_selector?('#t_number')

    subbut = find('input[type="submit"]')
    inp = find('#t_number')

    assert_equal 'Найти', subbut['value']

    inp.fill_in with: '5000'
    subbut.click
    sleep 10 until page.has_selector?('table.result')

    assert true, page.has_selector?('table.result')
    assert_equal 4, page.all('table tr').count

    find('button').click
    assert page.has_selector?('input[type="submit"]')
  end
end
