# frozen_string_literal: true

require_relative './program'
require 'English'

loop do
  puts "Выберите номер задания: [1], [2]\nИли напишите [выход], чтобы выйти"
  case gets.chomp
  when '1'
    puts 'Производится генерация файла и поиск слов'
    PartOne.emulate
    puts 'Процедура завершена'
  when '2'
    puts 'Введите радиус: '
    rad = gets.to_f
    puts 'Введите координаты: '
    coords = %i[x y z].zip(gets.scan(/(\d+(?:\.\d+)?)/).flatten.map(&:to_f)).to_h
    circle = PartTwo::Circle.new rad, coords
    puts "#{circle} with area #{circle.area}"
    circle = PartTwo::Sphere.new rad, coords
    puts "#{circle} with volume #{circle.volume}"
  when 'выход'
    puts 'Завершаю работу'
    break
  else
    puts 'Некорректный выбор'
  end
  puts('=' * 15)
end
