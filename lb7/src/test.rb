# frozen_string_literal: true

require 'minitest/autorun'

require_relative './program'

# Unit testing
# This code smells of :reek:InstanceVariableAssumption
class ProgramTest < Minitest::Test
  # This code smells of :reek:DuplicateMethodCall
  def setup
    @center = { x: 0, y: 0, z: 0 }.map { |key, _| [key, Random.rand(1..10_000)] }.to_h
    @radius = Random.rand(1..10_000)
    assert(File.exist?('F.txt'))
    assert(File.exist?('H.txt'))
  end

  # This code smells of :reek:FeatureEnvy
  # This code smells of :reek:TooManyStatements
  def test_part_one
    3.times do
      f_file, h_file = %w[F H].map { |name| File.open "#{name}.txt", 'w+' }
      batch = (1..10_000).map { PartOne.russian_roulette }
      f_file.write batch.join(' ')
      PartOne.capture_to_file_h f_file, h_file
      h_file.seek 0
      assert_equal((batch.filter { |word| word.match?(/[[:word:]]*[аa][[:word:]]*/) }).join(' '), h_file.read)
      [f_file, h_file].map(&:close)
    end
  end

  def test_part_two_circle
    t_c = PartTwo::Circle.new(@radius, @center)

    assert_equal(t_c.to_s, "Circle in (#{@center[:x]}, #{@center[:y]}) with radius = #{@radius}")
    assert_equal(t_c.area, (@radius**2) * Math::PI)
  end

  def test_part_two_sphere
    t_s = PartTwo::Sphere.new(@radius, @center)

    assert_equal(t_s.to_s,
                 "Sphere in (#{@center[:x]}, #{@center[:y]}, #{@center[:z]}) with radius = #{@radius}")
    assert_in_delta(t_s.volume, Rational(4, 3) * Math::PI * (@radius**3), 10**-5)
    assert(PartTwo::Sphere < PartTwo::Circle)
  end
end
