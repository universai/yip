# frozen_string_literal: true

# PartOne is for part one
module PartOne
  ALPHA = ('А'..'я').to_a + ('0'..'9').to_a + ['_'] + ('A'..'Z').to_a + ('a'..'z').to_a

  # This code smells of :reek:UncommunicativeVariableName
  # @param f_file [File]
  # @param h_file [File]
  # @return [Array(nil, nil)]
  def self.emulate(f_file = File.open('F.txt', 'w+'), h_file = File.open('H.txt', 'w+'))
    setup_file_f f_file
    capture_to_file_h f_file, h_file
    [f_file, h_file].map(&:close)
  rescue StandardError => e
    puts "Что-то пошло не так: #{e.message}"
  end

  # @param f_file [File]
  def self.setup_file_f(f_file)
    10_000.times { f_file.write "#{russian_roulette} " }
    f_file.truncate(f_file.size - 1)
  end

  # @param f_file [File]
  # @param h_file [File]
  def self.capture_to_file_h(f_file, h_file)
    f_file.seek 0
    cont = f_file.read.scan(/\b([[:word:]]*[аa][[:word:]]*)\b/)
    raise StandardError, "Не обнаружено слов с буквой 'а'" if cont.empty?

    h_file.write cont.flatten.join(' ')
  end

  def self.russian_roulette
    (Random.new.rand(1..30).times.map { ALPHA.sample }).join
  end
end

# PartTwo is for part two
module PartTwo
  # Circle describes round figure
  class Circle
    # @param radius [Numeric] Is a radius of circle
    # @param center [Hash<Symbol, Numeric>] Is a (x, y) coords of a circle
    def initialize(radius = 0, center = { x: 0, y: 0 })
      raise ArgumentError, 'Wrong [radius] should be Number' unless radius.is_a? Numeric
      raise ArgumentError, 'Wrong [center] should be a hash' unless center.is_a? Hash
      raise ArgumentError, '[center] should have [x] and [y]' unless (center.key? :x) && (center.key? :y)

      @center = { x: center[:x], y: center[:y] }
      @radius = radius
    end

    def to_s
      "Circle in (#{@center.values.join(', ')}) with radius = #{@radius}"
    end

    def area
      Math::PI * (@radius**2)
    end
  end

  # Sphere describes spheric figure
  # This code smells of :reek:InstanceVariableAssumption
  class Sphere < Circle
    undef_method(:area)

    def initialize(radius = 0, center = { x: 0, y: 0, z: 0 })
      raise ArgumentError, '[center] should have [z]' unless center.key? :z

      super(radius, center)
      @center.merge!({ z: center[:z] })
    end

    def volume
      Rational(4, 3) * Math::PI * (@radius**3)
    end

    def to_s
      super.sub('Circle', 'Sphere')
    end
  end
end
