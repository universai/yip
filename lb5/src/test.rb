# frozen_string_literal: true

require 'minitest/autorun'
require_relative './program'

# Unit testing
class TestProgram < Minitest::Test
  def self.generator
    (1..Random.new.rand(2..50)).map { Random.new.rand(-9999..9999) }
  end

  # This code smells of :reek:TooManyStatements
  # This code smells of :reek:NestedIterators
  def self.sorter(array)
    res = []
    array.map! do |arr|
      arr.split(', ').map! do |elem|
        elm = elem.split(' ').map(&:to_i)
        res[elm.first] = elm.last
        elm
      end
    end
    res
  end

  def test_part_one
    assert_in_delta 0.53, PartOne.calculate_y(0.9), 0.01
    assert_in_delta 0.56, PartOne.calculate_y(-1.6), 0.01
    assert_in_delta 0.000119, PartOne.calculate_y(20.0017), 0.00001
  end

  def test_part_two
    assert_equal (PartTwo.super_search %w[a b c d e f], [2, 6, 1, 4, 5, 8]),
                 ['d', %w[f b], %w[a b d e f], [2, 6, 4, 5, 8]]
    assert_equal (PartTwo.super_search %w[a b c d e f g h], [17, 32, 82, 14, 49, 43, 76, 13]),
                 ['f', %w[c g], %w[a b c d e f g], [17, 32, 82, 14, 49, 43, 76]]
    assert_equal (PartTwo.super_search %w[a b c d], [1, 2, 3, 4]),
                 ['b', %w[d c], %w[b c d], [2, 3, 4]]
  end

  # This code smells of :reek:TooManyStatements
  def test_part_three
    3.times do
      cls = self.class
      gen = cls.generator
      srt = PartThree.even_odd gen
      assert_equal gen, cls.sorter(srt)
      assert(srt[0].all? { |elem| elem.last.even? } && srt[1].all? { |elm| elm.last.odd? })
    end
  end
end
