# frozen_string_literal: true

require_relative './program'

loop do
  puts "Выберите номер задания: [1], [2], [3]\nИли напишите [выход], чтобы выйти"
  case gets.chomp
  when '1'
    puts 'Введите x:'
    puts "Результат вычисления функции: #{PartOne.calculate_y gets.to_f}"
  when '2'
    puts 'Введите массив фамилий:'
    inp = gets.scan(/(?:([А-Яа-яA-Za-z_-]+))/).flatten
    puts 'Введите массив зарплат:'
    res = PartTwo.super_search(inp, gets.scan(/(?:(\d+))+/).flatten.map(&:to_i))
    puts "Самая близкая к медианной зарплата у #{res[0]}\n" \
    "Самые большие зарплаты у #{res[1][0]} и #{res[1][1]}\n" \
    "Новые списки: #{res[2].inspect}, #{res[3].inspect}"
  when '3'
    puts 'Введите целочисленный массив:'
    res = PartThree.even_odd gets.scan(/(?:(\d+))+/).flatten.map(&:to_i)
    puts "Четные в формате индекс - элемент: #{res[0]}\n" \
    "Нечетные в формате индекс - элемент: #{res[1]}"
  when 'выход'
    puts 'Завершаю работу'
    break
  else
    puts 'Некорректный выбор'
  end
  puts('=' * 15)
end
