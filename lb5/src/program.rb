# frozen_string_literal: true

# PartOne is module for complex calc
module PartOne
  # @param x_var [Float]
  # @return [Float]
  def self.calculate_y(x_var)
    sup_sum = (x_var**2 + x_var) * x_var
    (
      (Math.sin(sup_sum.abs)**3) / \
      ((sup_sum - x_var)**2 + Math::PI)
    )**0.5
  end
end

# PartTwo is module for searching
module PartTwo
  # @param names [Array<String>] Will be modified
  # @param salaries [Array<Float>]
  # @return [Array(String, Array(String, String))] Will be modified
  def self.super_search!(names, salaries)
    res = [find_min_mean_man(names, salaries), find_max_sal_men(names, salaries)]

    min_sal_man = salaries.each_with_index.min.last
    salaries.delete_at min_sal_man
    names.delete_at min_sal_man

    res
  end

  # @param names [Array<String>]
  # @param salaries [Array<Float>]
  # @return [Array(String, Array(String, String), Array<String>, Array<Float>)]
  def self.super_search(names, salaries)
    nms = names.dup
    slr = salaries.dup
    (super_search! nms, slr).concat([nms, slr])
  end

  def self.find_min_mean_man(names, salaries)
    mean = Float(salaries.sum) / salaries.size
    names.at (salaries.map { |sal| (sal - mean).abs }).each_with_index.min[1]
  end

  def self.find_max_sal_men(names, salaries)
    salars = salaries.each_with_index
    salars.max(2).collect(&:last).map { |ind| names.at ind }
  end
end

# PartTwo is module for sorting
module PartThree
  # @param arr [Array<Integer>]
  def self.concrete(arr, cas)
    ((arr.filter { |elem| elem.first.send(cas) }).map { |tuple| "#{tuple.last} #{tuple.first}" }).join(', ')
  end

  # @param arr [Array<Integer>]
  # @return [Array(Array<Integer>, Array<Integer>)]
  def self.even_odd(arr)
    arr = arr.dup.each_with_index.sort
    %i[even? odd?].map { |cas| concrete(arr, cas) }
  end
end
